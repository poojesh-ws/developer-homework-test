import {
  GetProductsForIngredient,
  GetRecipes,
} from "./supporting-files/data-access";
import {
  Product,
  Recipe,
  RecipeLineItem,
  SupplierProduct,
} from "./supporting-files/models";
import { GetCostPerBaseUnit } from "./supporting-files/helpers";
import { RunTest, ExpectedRecipeSummary } from "./supporting-files/testing";
import { CompleteRecipeSummary } from "./models/RecipeSummary";
import {
  addNutrientFactToRecipeSummary,
  nutrientFactFactory,
} from "./utils";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

/**
 * This function calculates the cheapest configuration cost for all recipes that are sent to it
 * @date 2023-01-09
 * @param {Recipe[]} recipeList
 * @param {CompleteRecipeSummary} recipeSummary
 * @returns {CompleteRecipeSummary}
 */
function calculateCheapestConfiguration(
  recipeList: Recipe[],
  recipeSummary: CompleteRecipeSummary
): CompleteRecipeSummary {
  const recipeConfigurationSummary: CompleteRecipeSummary = recipeSummary;

  // iterate over all the recipes in the list
  recipeList.forEach((recipe) => {
    // create the skeleton of the output object
    recipeConfigurationSummary[recipe.recipeName] = {
      cheapestCost: 0,
      nutrientsAtCheapestCost: {
        ...nutrientFactFactory("Carbohydrates"),
        ...nutrientFactFactory("Fat"),
        ...nutrientFactFactory("Protein"),
        ...nutrientFactFactory("Sodium"),
      },
    };
    // iterate over recipeLineItems and find out cheapest supplier
    recipe.lineItems.forEach((recipeLineItem: RecipeLineItem) => {
      // get all products for a particular ingredient 
      const products = GetProductsForIngredient(recipeLineItem.ingredient);
      
      // assign a base cheap value as the first element in the array. 
      // This will prevent n2 iterations to find out the cheapest supplier
      let cheapestProduct = products[0];
      let cheapestSupplierProduct = cheapestProduct.supplierProducts[0];
      let cheapestPrice = GetCostPerBaseUnit(cheapestSupplierProduct);

      // iterate over products and find the cheapest supplier
      products.forEach((product: Product) => {
        product.supplierProducts.forEach((supplierProduct: SupplierProduct) => {
          const supplierProductPrice = GetCostPerBaseUnit(supplierProduct);
          if (cheapestPrice > supplierProductPrice) {
            cheapestSupplierProduct = supplierProduct;
            cheapestPrice = supplierProductPrice;
            cheapestProduct = product;
          }
        });
      });
      // aggregate cost
      recipeConfigurationSummary[recipe.recipeName].cheapestCost +=
        cheapestPrice * recipeLineItem.unitOfMeasure.uomAmount;

      // aggregate nutrientFacts
      cheapestProduct.nutrientFacts.forEach((nutrientFact) => {
        recipeConfigurationSummary[
          recipe.recipeName
        ].nutrientsAtCheapestCost = addNutrientFactToRecipeSummary(
          recipeConfigurationSummary[recipe.recipeName].nutrientsAtCheapestCost,
          nutrientFact
        );
      });
    });
  });
  return recipeConfigurationSummary;
}
calculateCheapestConfiguration(recipeData, recipeSummary);
/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */

RunTest(recipeSummary);
