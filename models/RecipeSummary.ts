import { NutrientFact } from "./../supporting-files/models";
export interface CompleteRecipeSummary {
  [key: string]: RecipeSummary;
}
export interface RecipeSummary {
  cheapestCost: number;
  nutrientsAtCheapestCost: NutrientsAtCheapestCost;
}
export interface NutrientsAtCheapestCost {
  [key: string]: NutrientFact;
}
