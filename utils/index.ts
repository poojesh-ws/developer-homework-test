import { NutrientsAtCheapestCost } from "../models/RecipeSummary";
import { GetNutrientFactInBaseUnits } from "../supporting-files/helpers";
import { NutrientFact, UoMName, UoMType } from "../supporting-files/models";

/**
 * Accepts a nutrientFact converts it into the base unit and aggregates the values
 * to the passed in nutrientsAtCheapestCost object
 * @date 2023-01-06
 * @param {NutrientsAtCheapestCost} nutrientsAtCheapestCost
 * @param {NutrientFact} nutrientFact
 * @returns {NutrientsAtCheapestCost}
 */
export function addNutrientFactToRecipeSummary(
  nutrientsAtCheapestCost: NutrientsAtCheapestCost,
  nutrientFact: NutrientFact
): NutrientsAtCheapestCost {
  const nutrientFactInBaseUnit = GetNutrientFactInBaseUnits(nutrientFact);
  nutrientsAtCheapestCost[nutrientFactInBaseUnit.nutrientName] = {
    ...nutrientFactInBaseUnit,
    quantityAmount: {
      ...nutrientFactInBaseUnit.quantityAmount,
      uomAmount:
        nutrientsAtCheapestCost[nutrientFactInBaseUnit.nutrientName]
          .quantityAmount.uomAmount +
        nutrientFactInBaseUnit.quantityAmount.uomAmount,
    },
  };
  return nutrientsAtCheapestCost;
}

/**
 * Accepts the nutrient name and generates a base NutrientFactory object
 * @date 2023-01-09
 * @param {string} nutrientName
 * @returns {NutrientsAtCheapestCost}
 */
export function nutrientFactFactory(
  nutrientName: string
): NutrientsAtCheapestCost {
  return {
    [nutrientName]: {
      nutrientName,
      quantityAmount: {
        uomAmount: 0,
        uomName: UoMName.grams,
        uomType: UoMType.mass,
      },
      quantityPer: {
        uomAmount: 0,
        uomName: UoMName.grams,
        uomType: UoMType.mass,
      },
    },
  };
}
